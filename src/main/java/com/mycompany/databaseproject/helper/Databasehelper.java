/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Databasehelper {

    private static Connection conn = null;
    private static String URL = "jdbc:sqlite:dcoffee.db";
    static{
       getConnection();
    }
    public static synchronized Connection getConnection(){
       if(conn==null){
           try {
               conn = DriverManager.getConnection(URL);
               System.out.println("Connection to Sqllite has been establish.");
           } catch (SQLException ex) {
               Logger.getLogger(Databasehelper.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
      
       return conn;
    }
    public static synchronized void close(){
         if(conn!=null){
             try {
                 conn.close();
                 conn= null;
             } catch (SQLException ex) {
                 Logger.getLogger(Databasehelper.class.getName()).log(Level.SEVERE, null, ex);
             }
         }
    }
    public static int getinsertId(Statement stmt){
        try {
            ResultSet key = stmt.getGeneratedKeys();
            key.next();
            return key.getInt(1);
        } catch (SQLException ex) {
            Logger.getLogger(Databasehelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
}
